# Architecture Lab
A compilation of my experiments for AUC's Architecture Lab course.


# Usage
Every folder has its own Readme.md with instructions.
## Linux / Mac
Use the bash script to build all verilog files:
```bash
    ./buildall.sh
```

# Requirements
IcarusVerilog, GTKWave

# License
N/A.