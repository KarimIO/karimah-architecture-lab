`timescale 1ns/1ns

`include "cpu.v"
`include "../Lab4/assignment/rom.v"
`include "../Lab4/assignment/ram.v"

module computer(clk, rst);
    input clk, rst;

    wire [31:0]inst, AluResult, RD2, PC, ReadData;
    wire MemWrite;

    cpu core(.clk(clk), .rst(rst), .PC(PC), .inst(inst), .MemWrite(MemWrite), .AluResult(AluResult), .RD2(RD2), .ReadData(ReadData));
    rom #(18) rommunit(.adr(PC), .dout(inst));
    ram rammunit(.clk(clk), .we(MemWrite), .adr(AluResult), .din(RD2), .dout(ReadData));
endmodule