`timescale 1ns/1ns

`include "../Lab4/alu.v"
`include "../Lab4/registerfile32.v"
`include "../Lab4/signext.v"
`include "../Lab4/lshift.v"

module datapath(clk, rst, inst, MemToReg, AluControl, AluSrc, RegDst, RegWrite, J, Branch, AluResult, RD2, ReadData, PC);
    input clk, rst;
    output [31:0]inst;
    input [3:0] AluControl;
    input MemToReg, AluSrc, RegDst, RegWrite, J, Branch;

    input [31:0]ReadData;
    output [31:0]AluResult, RD2, PC;

    wire Zero;

    wire [4:0] WriteReg;
    wire [31:0]RD1, SignImm, SrcB, Result, ShiftImm, PC, PCBranch, PCB, PCPlus4, PCJ;
    wire [4:0]shamt;

    mux2 #(5) muxWriteReg(.d0(inst[20:16]), .d1(inst[15:11]), .s(RegDst), .y(WriteReg));
    registerfile32 rf(.clk(clk), .we(RegWrite), .ra1(inst[25:21]), .ra2(inst[20:16]), .wa(WriteReg), .wd(Result), .rd1(RD1), .rd2(RD2));

    signext se(.a(inst[15:0]), .y(SignImm));
    mux2 muxALUSrc(.d0(RD2), .d1(SignImm), .s(AluSrc), .y(SrcB));
    alu #(32) alu32(.a(RD1), .b(SrcB), .f(AluControl), .shamt(5'b0), .y(AluResult), .zero(Zero));
    lshift ls(.a(SignImm), .y(ShiftImm));

    mux2 res(.d1(ReadData), .d0(AluResult), .s(MemToReg), .y(Result));

    wire o;
    adder pcb(.a(ShiftImm), .b(PCPlus4), .subAdd(1'b0), .y(PCBranch), .overflow(o));

    wire [31:0]shiftedJ;
    lshift lsj(.a({6'b0, inst[25:0]}), .y(shiftedJ));

    wire PCSrc;
    and a(PCSrc, Branch, Zero);
    mux2 mb(.d0(PCPlus4), .d1(PCBranch), .s(PCSrc), .y(PCB));
    mux2 mj(.d0(PCB), .d1({PCPlus4[31:28], shiftedJ[27:0]}), .s(J), .y(PCJ));
    flopr_param ff(.clk(clk), .rst(rst), .d(PCJ), .q(PC));
    adder pc4(.a(PC), .b(4), .subAdd(1'b0), .y(PCPlus4), .overflow(o));
endmodule