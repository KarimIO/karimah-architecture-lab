`timescale 1ns/1ns

`include "datapath.v"
`include "controlpath.v"

module cpu(clk, rst, PC, inst, MemWrite, AluResult, RD2, ReadData);
    input clk, rst;
    output [31:0]inst, AluResult, RD2, PC;
    input [31:0]ReadData;
    output MemWrite;

    wire [3:0] AluControl;
    wire MemToReg, AluSrc, RegDst, RegWrite, J, Branch;

    controlpath cp(inst, MemToReg, MemWrite, AluControl, AluSrc, RegDst, RegWrite, J, Branch);
    datapath    dp(clk, rst, inst, MemToReg, AluControl, AluSrc, RegDst, RegWrite, J, Branch, AluResult, RD2, ReadData, PC);
endmodule