`timescale 1ns/1ns

`include "computer.v"

module computer_tb();
    reg clk, rst;
    computer UUT(clk, rst);

    always #5 clk = ~clk;

    initial begin
        clk = 1;
        rst = 1;
        #10;
        rst = 0;
        #150;
        $finish;
    end

    initial begin
        $dumpfile("output/computer.vcd");
        $dumpvars;
    end
endmodule