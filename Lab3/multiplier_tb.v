`timescale 1ns/1ns

`include "multiplier.v"

module ArrayMultiplierTB();
    parameter width = 32;
    reg [width-1:0] A, B;
    wire [width*2-1:0] O;
    ArrayMultiplier #(width) UUT(.A(A), .B(B), .O(O));

    wire [width*2-1:0] checker;
    wire error;
    integer i;
    initial begin
        A = 0;
        B = 0;
        for(i = 0; i < 15; i++) begin
            A = $random;
            B = $random;
            #5;
        end
    end

    assign checker = A * B;
    assign error = ~(checker == O);

    initial begin
        $dumpfile("output/dump.vcd");
        $dumpvars;
    end
endmodule