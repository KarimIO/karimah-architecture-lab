`timescale 1ns/1ns

module HA(A, B, S, Cout);
    input A, B;
    output S, Cout;

    xor(S, A, B);
    and(Cout, A, B);
endmodule

module FA(A, B, Cin, S, Cout);
    input A, B, Cin;
    output S, Cout;

    wire w1, w2, w3;
    xor(w1, A, B);
    and(w2, A, B);
    and(w3, w1, Cin);
    xor(S, w1, Cin);
    or(Cout, w2, w3);
endmodule

module ArrayMultiplier(A, B, O);
    parameter width = 32;
    input [width-1:0] A, B;
    output [width*2-1:0] O;

    and a00(O[0], A[0], B[0]);

    wire [width-1:0] rowWires [width-1:0];
    wire [width-1:0] colWires [width-1:0];

    // First Row
    // First Half Adder of first row
    wire ha1_1_w1;
    wire ha1_1_w2;
    and ha1_1_a1(ha1_1_w1, A[1], B[0]);
    and ha1_1_a2(ha1_1_w2, A[0], B[1]);
    HA ha1_1(.A(ha1_1_w1), .B(ha1_1_w2), .S(O[1]), .Cout(rowWires[0][0]));

    // Full Adders of first row
    genvar i, j;
    generate
        for (i = 1; i < width-1; i = i + 1) begin: firstLoop
            wire fa1_1_w1, fa1_1_w2;
            and fa1_1_a1(fa1_1_w1, A[i+1], B[0]);
            and fa1_1_a2(fa1_1_w2, A[i], B[1]);
            FA fa(.A(fa1_1_w1), .B(fa1_1_w2), .Cin(rowWires[0][i-1]), .S(colWires[0][i-1]), .Cout(rowWires[0][i]));
        end
    endgenerate

    // Last Half Adder of first row
    wire ha1_2_w;
    and ha1_2_a1(ha1_2_w, A[width-1], B[1]);
    HA ha1_2(.A(ha1_2_w), .B(rowWires[0][width-2]), .S(colWires[0][width-2]), .Cout(colWires[0][width-1]));

    // All other Rows
    generate
        for (i = 1; i < width-1; i = i + 1) begin: largeLoop
            wire hw;
            and haa(hw, A[0], B[i+1]);
            HA h(.A(colWires[i-1][0]), .B(hw), .S(O[i+1]), .Cout(rowWires[i][0]));
            
            for (j = 1; j < width; j = j + 1) begin: smallLoop
                wire fw;
                and fa(fw, A[j], B[i+1]);
                if (i == width-2)
                    if (j == width-1)
                        FA f(.A(fw), .B(colWires[i-1][j]), .Cin(rowWires[i][j-1]), .S(O[j+width-1]), .Cout(O[j+width]));
                    else
                        FA f(.A(fw), .B(colWires[i-1][j]), .Cin(rowWires[i][j-1]), .S(O[j+width-1]), .Cout(rowWires[i][j]));
                else
                    if (j == width-1)
                        FA f(.A(fw), .B(colWires[i-1][j]), .Cin(rowWires[i][j-1]), .S(colWires[i][j-1]), .Cout(colWires[i][j]));
                    else
                        FA f(.A(fw), .B(colWires[i-1][j]), .Cin(rowWires[i][j-1]), .S(colWires[i][j-1]), .Cout(rowWires[i][j]));
            end
        end
    endgenerate
endmodule