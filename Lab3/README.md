# Architecture Lab 3 - Array Multiplier
A parameterized array multiplier in verilog.

# Usage
## Windows
Compile multiplier_tb.v in iVerilog. I'll upload a .bat file to do this automatically later.

## Linux / Mac
Use the bash script:
```bash
    ./build.sh
```

To view the waveform, either open the generated output/dump.vcd file in GTKWave, or use the following shell script:
```bash
    ./buildrun.sh
```

# Requirements
IcarusVerilog, GTKWave

# License
N/A.