`timescale 1ns/1ns

`include "cpu.v"
`include "../Lab4/assignment/rom.v"
`include "../Lab4/assignment/ram.v"

module computer(clk, rst);
    input clk, rst;

    wire [31:0]inst, MemAddr, ReadDataM, PC, WriteDataM;
    wire MemWrite;
    
    cpu core(.clk(clk), .rst(rst), .PC(PC), .InstrF(inst), .MemWriteM(MemWrite), .MemAddr(MemAddr), .WriteDataM(WriteDataM), .ReadDataM(ReadDataM));
    rom #(18) rommunit(.adr(PC), .dout(inst));
    ram rammunit(.clk(clk), .we(MemWrite), .adr(MemAddr), .din(WriteDataM), .dout(ReadDataM));
endmodule