module datapath (clk, InstrF, InstrD, MemWriteD, MemWriteM, MemToRegD, ALUControlD, ALUSrcD, RegDstD, RegWriteD, BranchD, ReadDataM, ALUOutM, WriteDataM, PCF);
    input clk;
    input [31:0]InstrF;
    
    input [31:0] ReadDataM;
    input [ 2:0] ALUControlD;
    input MemToRegD, MemWriteD, BranchD, ALUSrcD, RegDstD, RegWriteD;
    
    output reg [31:0] InstrD;
    output reg [31:0] ALUOutM, WriteDataM;
    output reg [31:0] PCF;
    output reg MemWriteM;

    // Control Path Wires
    reg [31:0] WriteDataE, RD1E;
    reg [31:0] ReadDataW;
    reg        RegWriteE, RegWriteM, RegWriteW;
    reg        MemToRegE, MemToRegM, MemToRegW;
    reg        MemWriteE;
    reg        BranchE, BranchM;
    reg [ 2:0] ALUControlE;
    reg ALUSrcE;
    reg RegDstE;

    reg [31:0]PCPlus4D, PCPlus4E;
    reg [31:0]ALUOutE, ALUOutW;
    reg [31:0]SignImmE;

    reg [4:0]RtE, RdE, WriteRegM, WriteRegW;

    // Register File
    reg [31:0] rf[31:0];
    
    wire [4:0]WriteRegE;
    wire [31:0] rd1, rd2, SrcAE, SrcBE;
    wire [31:0]ResultW, SignImmD, Shifted;
    wire PCSrcM;
    wire [31:0]PC, PCBranchM, PCPlus4F;

    // ===============================
    // Continuous
    // ===============================
    // Regfile
    assign rd1 = rf[InstrD[25:21]];
    assign rd2 = rf[InstrD[20:16]];

    // PC
    assign PCSrcM = BranchM && ZeroM;
    assign PC = (PCSrcM) ? PCBranchM : PCPlus4F;
    assign PCBranchM = PCPlus4E + Shifted;
    assign PCPlus4F = PCF + 4;

    // RegWriteD
    assign WriteRegE = RegDstE ? RdE : RtE;

    // Hazard Unit
    wire [31:0] forwardDataA, forwardDataB;
    assign SrcAE = ((WriteRegM && RsE == WriteDataM) ? ALUOutM : ((WriteRegW && RsE == WriteDataW) ? ALUOutW : RD1E ) );
    assign forwardDataB = ((WriteRegM && RtE == WriteDataM) ? ALUOutM : ((WriteRegW && RtE == WriteDataW) ? ALUOutW : WriteDataE ) );

    // ALU
    assign SrcBE = ALUSrcE ? SignImmE : forwardDataA;

    assign ResultW = MemToRegW ? ReadDataW : ALUOutW;

    assign SignImmD[31:16] = {16{a[15]}};
    assign SignImmD[15:0] = instrD[15:0];

    assign Shifted = SignImmD << 2;

    always @ (SrcAE, SrcBE, ALUControlE) begin
        case (ALUControlE)
            3'b010: // add
                ALUOutE <= (SrcAE + SrcBE);
            3'b110: // sub
                ALUOutE <= (SrcAE - SrcBE);
            3'b000: // and
                ALUOutE <= (SrcAE & SrcBE);
            3'b001: // or
                ALUOutE <= (SrcAE | SrcBE);
            3'b111: // slt
                ALUOutE <= (SrcAE < SrcBE)?1:0;
        endcase
    end

    assign ZeroM = (ALUOutE == 0);

    // ===============================
    // Stages
    // ===============================
    always @ (posedge clk) begin
        PCF = PC;
    end

    always @ (posedge clk) begin
        InstrD   <= InstrF;
        PCPlus4D <= PCPlus4F;
    end

    always @ (posedge clk) begin
        RegWriteE   <= RegWriteD;
        MemToRegE   <= MemToRegD;
        MemWriteE   <= MemWriteD;
        BranchE     <= BranchD;
        ALUControlE <= ALUControlD;
        ALUSrcE     <= ALUSrcD;
        RegDstE     <= RegDstD;

        PCPlus4E    <= PCPlus4D;

        SignImmE    <= SignImmD;

        WriteDataE  <= rd2;
        RD1E <= rd1;
    end

    always @ (posedge clk) begin
        RegWriteM  <= RegWriteE;
        MemToRegM  <= MemToRegE;
        MemWriteM  <= MemWriteE;
        BranchM    <= BranchE;
        WriteRegM  <= WriteRegE;
        WriteDataM <= WriteDataE;
    end

    always @ (posedge clk) begin
        RegWriteW <= RegWriteM;
        MemToRegW <= MemToRegM;
        WriteRegW <= WriteRegM;
        ReadDataW <= ReadDataM;
    end

    always @ (posedge clk) begin
        if (RegWriteW)
            rf[WriteRegW] <= ResultW;

        rf[0] <= 0;
    end

endmodule