module hazard(
	input [4:0] rsD, rtD, rsE, rtE,
	input [4:0] writeregE, writeregM, writeregW,
	input regwriteE, regwriteM, regwriteW,
	input memtoregE, memtoregM, branchD,
	output reg [1:0] forwardaE, forwardbE,
	output stallF, stallD, flushE
);
	always @ (*) begin
		// SRCA
		if (regwriteM && rsE == writeregM) begin
			forwardaE = 2'b10;
		end else if (regwriteW && rsE == writeregW) begin
			forwardaE = 2'b01;
		end else begin
			forwardaE = 2'b00;
		end

		// SRCB
		if (regwriteM && rtE == writeregM) begin
			forwardbE = 2'b10;
		end else if (regwriteW && rtE == writeregW) begin
			forwardbE = 2'b01;
		end else begin
			forwardbE = 2'b00;
		end
	end

	wire lwstall;
	assign lwstall = ((rsE==rtD) | (rtE==rsD)) & memtoregE;
	assign flushE = lwstall;
	assign stallF = lwstall;
	assign stallD = lwstall;

endmodule