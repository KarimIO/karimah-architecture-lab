`timescale 1ns/1ns

`include "datapath.v"
`include "controlpath.v"

module cpu(clk, rst, PC, InstrF, WriteDataM, MemAddr, MemWriteM, ReadDataM);
    input clk, rst;
    input [31:0] InstrF;
    output [31:0] MemAddr, PC;
    input [31:0]ReadDataM;
    output [31:0]WriteDataM;
    output MemWriteM;

    wire [31:0] InstrD;
    wire [2:0] ALUControlD;
    wire MemWriteD, MemToRegD, ALUSrcD, RegDstD, RegWriteD, BranchD;

    controlpath cp(InstrD, MemToRegD, MemWriteD, ALUControlD, ALUSrcD, RegDstD, RegWriteD, BranchD);
    datapath    dp(clk, InstrF, InstrD, MemWriteD, MemWriteM, MemToRegD, ALUControlD, ALUSrcD, RegDstD, RegWriteD, BranchD, ReadDataM, MemAddr, WriteDataM, PC);
endmodule