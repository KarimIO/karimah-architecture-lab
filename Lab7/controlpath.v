`timescale 1ns/1ns

module controlpath(inst, MemToReg, MemWrite, AluControl, AluSrc, RegDst, RegWrite, Branch);
    input [31:0]inst;
    output reg [2:0] AluControl;
    output reg MemToReg, MemWrite, Branch, AluSrc, RegDst, RegWrite;

    reg [1:0] ALUOp;

    initial begin
        RegWrite = 0;
        RegDst = 0;
        AluSrc = 0;
        Branch = 0;
        MemWrite = 0;
        MemToReg = 0;
    end

    always @ (inst) begin
        // Opcode
        case(inst[31:26])
            6'b000000: begin // R-Type
                RegWrite = 1;
                RegDst = 1;
                AluSrc = 0;
                Branch = 0;
                MemWrite = 0;
                MemToReg = 0;
                ALUOp = 2'b10;
            end
            6'b100011: begin // lw
                RegWrite = 1;
                RegDst = 0;
                AluSrc = 1;
                Branch = 0;
                MemWrite = 0;
                MemToReg = 1;
                ALUOp = 2'b00;
            end
            6'b101011: begin // sw
                RegWrite = 0;
                //RegDst = 0; // X
                AluSrc = 1;
                Branch = 0;
                MemWrite = 1;
                //MemToReg = 0; // X
                ALUOp = 2'b00;
            end
            6'b000100: begin // beq
                RegWrite = 0;
                //RegDst = 0; // X
                AluSrc = 0;
                Branch = 1;
                MemWrite = 0;
                //MemToReg = 0; // X
                ALUOp = 2'b01;
            end
            6'b001000: begin // addi
                RegWrite = 1;
                RegDst = 0;
                AluSrc = 1;
                Branch = 0;
                MemWrite = 0;
                MemToReg = 0;
                ALUOp = 2'b00;
            end
        endcase

        if (ALUOp == 2'b00)
            AluControl = 3'b010; // Add
        else if (ALUOp == 2'b01)
            AluControl = 3'b110; // Sub
        else begin
            // Funct
            case(inst[5:0])
                6'b100000: AluControl = 3'b010; // add
                6'b100010: AluControl = 3'b110; // sub
                6'b100100: AluControl = 3'b000; // and
                6'b100101: AluControl = 3'b001; // or
                6'b101010: AluControl = 3'b111; // slt
            endcase
        end
    end
endmodule