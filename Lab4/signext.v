`timescale 1ns/1ns

module signext(a, y);
    parameter in=16;
    parameter out=32;
    parameter diff=out-in;
    input [in-1:0]a;
    output [out-1:0]y;

    assign y[out-1:diff] = {diff{a[in-1]}};
    assign y[in-1:0] = a[in-1:0];
endmodule