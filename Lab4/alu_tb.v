`timescale 1ns/1ns

`include "../Lab4/alu.v"

module alu_tb();
    parameter width = 32;
    parameter bitLength = $clog2(width);

    // Inputs
    reg [width-1:0]a;
    reg [width-1:0]b;
    reg [bitLength-1:0]shamt;
    reg [3:0]f;

    // Outputs
    wire [width-1:0]y;
    wire zero;

    alu #(width) UUT(.a(a), .b(b), .f(f), .shamt(shamt), .y(y), .zero(zero));

    integer i;
    initial begin
        a = 0;
        b = 0;
        shamt = 0;
        f = 0;
        for(i = 0; i < 15; i++) begin
            a = $random;
            shamt = $random;
            f = $random;
            if (f == 15 || f == 17)
                b = $random % a; // Prevent overflow from subtract by keeping the b < a.
            else
                b = $random;
            #5;
        end
    end

    wire [width-1:0]checkerAS, checkerORXOR, checkerAND, checkerSA, checkerSL, checkerShift, checker;
    wire error, zeroError;
    assign checkerAS = f[0]?(a-b):(a+b);
    assign checkerORXOR = f[0]?(a^b):(a|b);
    assign checkerAND = a&b;
    assign checkerSA = f[0]?($signed(a)>>>shamt):($signed(a)<<<shamt);
    assign checkerSL = f[0]?(a>>shamt):(a<<shamt);
    assign checkerShift = f[1]?(checkerSA):(checkerSL);
    assign checker = (f[3])?(f[2]?checkerShift:checkerORXOR):(f[2]?checkerAND:checkerAS);
    assign error = ~(checker == y);
    assign zeroError = ~((y==0) == zero);

    initial begin
        $dumpfile("output/alu.vcd");
        $dumpvars;
    end
endmodule