`timescale 1ns/1ns

module lshift(a, y);
    parameter width = 32;
    input [width-1:0]a;
    output [width-1:0]y;
    assign y = { {(width-2){a[width-3:0]}}, 2'b00 };
endmodule