`timescale 1ns/1ns

module andp(y, a, b);
    parameter width = 32;
    input   [width-1:0]a;
    input   [width-1:0]b;
    output  [width-1:0]y;

    assign y = a & b;
endmodule

module orp(y, a, b);
    parameter width = 32;
    input   [width-1:0]a;
    input   [width-1:0]b;
    output  [width-1:0]y;

    assign y = a | b;
endmodule

module xorp(y, a, b);
    parameter width = 32;
    input   [width-1:0]a;
    input   [width-1:0]b;
    output  [width-1:0]y;

    assign y = a ^ b;
endmodule