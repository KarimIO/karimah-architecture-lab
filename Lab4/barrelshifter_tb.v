`timescale 1ns/1ns

`include "../Lab4/barrelshifter.v"

module BarrelShifterp_tb();
    parameter width = 32;
    parameter bitLength = $clog2(width);

    // Inputs
    reg [width-1:0]a;
    reg [bitLength-1:0]shamt;
    reg arith;
    reg right;

    // Outputs
    wire [width-1:0]o;

    BarrelShifterp #(width) UUT(.A(a), .O(o), .shamt(shamt), .arith(arith), .right(right));

    wire [width-1:0] checker;
    wire error;
    integer i;
    initial begin
        a = 0;
        shamt = 0;
        arith = 0;
        right = 0;
        for(i = 0; i < 60; i++) begin
            a = $random;
            shamt = $random;
            arith = $random;
            right = $random;
            #5;
        end
    end

    wire [width-1:0] checkerAL;
    wire [width-1:0] checkerAR;
    wire [width-1:0] checkerLL;
    wire [width-1:0] checkerLR;
    wire [width-1:0] checkerL;
    wire [width-1:0] checkerA;
    assign checkerAL = $signed(a) <<< shamt;
    assign checkerAR = $signed(a) >>> shamt;
    assign checkerLL = a << shamt;
    assign checkerLR = a >> shamt;
    assign checkerL = right ? checkerLR : checkerLL;
    assign checkerA = right ? checkerAR : checkerAL;
    assign checker = arith?checkerA:checkerL;
    assign error = ~(checker == o);

    initial begin
        $dumpfile("output/barrelshifter.vcd");
        $dumpvars;
    end
endmodule