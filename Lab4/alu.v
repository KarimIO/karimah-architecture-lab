`timescale 1ns/1ns

`include "../Lab4/adder.v"
`include "../Lab4/aluoperations.v"
`include "../Lab4/barrelshifter.v"

module alu(a, b, f, shamt, y, zero);
    parameter width = 32;
    parameter bitLength = $clog2(width);
    input   [width-1:0]a;
    input   [width-1:0]b;
    input   [3:0]f;
    input   [bitLength-1:0]shamt;
    output  [width-1:0]y;
    output  zero;

    wire [width-1:0]wadder;
    wire overflow;
    wire subAdd;
    adder #(width) addp(.a(a), .b(b), .subAdd(f[0]), .y(wadder), .overflow(overflow));

    wire [width-1:0]wandp;
    andp #(width) ap(wandp, a, b);

    wire [width-1:0]wslt;
    assign wslt = (a < b)?1:0;

    wire [width-1:0]wandslt;
    mux2 #(width) andsltmux(.d0(wandp), .d1(wslt), .s(f[0]), .y(wandslt));

    wire [width-1:0]worp;
    orp #(width) op(worp, a, b);

    wire [width-1:0]wxorp;
    xorp #(width) xp(wxorp, a, b);

    wire [width-1:0]worf;
    mux2 #(width) ormux(.d0(worp), .d1(wxorp), .s(f[0]), .y(worf));

    wire [width-1:0]wshifter;
    BarrelShifterp #(width) bs(.A(a), .O(wshifter), .shamt(shamt), .arith(f[1]), .right(f[0]));

    wire [width-1:0]preFinal;
    mux4 #(width) multiplexer(.d0(wadder), .d1(wandslt), .d2(worf), .d3(wshifter), .s(f[3:2]), .y(preFinal));

    assign zero = (preFinal==32'b0);
    assign y = preFinal;
endmodule