`timescale 1ns/1ns

module and4(o, a, b, c, d);
    input a, b, c, d;
    output o;

    wire w1, w2;
    and(w1, a, b);
    and(w2, c, d);
    and(o, w1, w2);
endmodule

module decoder4(a, y); 
    input [3:0]a;
    output [15:0]y;

    wire [3:0]ia;
    not n0(ia[0], a[0]);
    not n1(ia[1], a[1]);
    not n2(ia[2], a[2]);
    not n3(ia[3], a[3]);

    and4 a0(y[0], ia[0], ia[1], ia[2], ia[3]);
    and4 a1(y[1], ia[0], ia[1], ia[2], a[3]);
    and4 a2(y[2], ia[0], ia[1], a[2], ia[3]);
    and4 a3(y[3], ia[0], ia[1], a[2], a[3]);
    and4 a4(y[4], ia[0], a[1], ia[2], ia[3]);
    and4 a5(y[5], ia[0], a[1], ia[2], a[3]);
    and4 a6(y[6], ia[0], a[1], a[2], ia[3]);
    and4 a7(y[7], ia[0], a[1], a[2], a[3]);
    and4 a8(y[8], a[0], ia[1], ia[2], ia[3]);
    and4 a9(y[9], a[0], ia[1], ia[2], a[3]);
    and4 a10(y[10], a[0], ia[1], a[2], ia[3]);
    and4 a11(y[11], a[0], ia[1], a[2], a[3]);
    and4 a12(y[12], a[0], a[1], ia[2], ia[3]);
    and4 a13(y[13], a[0], a[1], ia[2], a[3]);
    and4 a14(y[14], a[0], a[1], a[2], ia[3]);
    and4 a15(y[15], a[0], a[1], a[2], a[3]);
endmodule