`timescale 1ns/1ns

`include "../Lab4/mux4.v"

module mux4_tb();

    // Inputs
    reg [31:0]a;
    reg [31:0]b;
    reg [31:0]c;
    reg [31:0]d;
    reg [1:0]s;

    // Outputs
    wire [31:0]y;

    mux4 #(32) UUT(.d0(a), .d1(b), .d2(c), .d3(d), .s(s), .y(y));

    integer i;
    initial begin
        a = 0;
        b = 0;
        c = 0;
        d = 0;
        s = 0;
        for(i = 0; i < 15; i++) begin
            a = $random;
            b = $random;
            c = $random;
            d = $random;
            s = $random;
            #10;
        end
    end

    wire [31:0]checker;
    wire error;
    assign checker = (s[1])?(s[0]?d:c):(s[0]?b:a);
    assign error = ~(checker == y);

    initial begin
        $dumpfile("output/mux4.vcd");
        $dumpvars;
    end
endmodule