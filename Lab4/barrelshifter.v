`timescale 1ns/1ns

// Use mux4.v rather than mux2.v due to shared includes
`include "../Lab4/mux4.v"

module BarrelShifterp(A, O, shamt, arith, right);
    parameter width = 32;
    parameter bitLength = $clog2(width);
    
    input [width-1:0] A;
    input [bitLength-1:0] shamt;
    input arith, right;
    output [width-1:0] O;
    
    wire lastbit;
    assign lastbit = (arith&&right)? A[width-1] : 1'b0;
    
    genvar i, j;
    wire [width-1:0] C [bitLength:0];
    
    generate
        for(i = 0; i < width; i=i+1) begin : reversal1
            mux2 #(1) rm(.d0(A[i]), .d1(A[width-1-i]), .s(right), .y(C[0][i]));
        end
    endgenerate

    generate
        for(i = 1; i <= bitLength; i=i+1) begin : barrelRow
            for(j = 0; j < width; j=j+1) begin : barrelBit
                if (j-(2**(i-1)) < 0)
                    mux2 #(1) rm(.d0(C[i-1][j]), .d1(lastbit), .s(shamt[i-1]), .y(C[i][j]));
                else
                    mux2 #(1) rm(.d0(C[i-1][j]), .d1(C[i-1][j-(2**(i-1))]), .s(shamt[i-1]), .y(C[i][j]));
            end
        end
    endgenerate
    
    generate
        for(i = 0; i < width; i=i+1) begin : reversal2
            mux2 #(1) rm(.d0(C[bitLength][i]), .d1(C[bitLength][width-1-i]), .s(right), .y(O[i]));
        end
    endgenerate
    
endmodule 