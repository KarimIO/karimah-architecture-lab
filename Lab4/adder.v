`timescale 1ns/1ns

module FA(A, B, Cin, S, Cout);
    input A, B, Cin;
    output S, Cout;

    wire w1, w2, w3;
    xor(w1, A, B);
    and(w2, A, B);
    and(w3, w1, Cin);
    xor(S, w1, Cin);
    or(Cout, w2, w3);
endmodule

module adder(a, b, subAdd, y, overflow);
    parameter width = 32;
    input   subAdd;
    input   [width-1:0]a, b;
    output  [width-1:0]y;
    output  overflow;

    wire [width-2:0]carry;
    genvar i;
    generate
        for (i = 0; i < width; i=i+1) begin: adderLoop
            wire bf;
            xor (bf, b[i], subAdd);
            if (i==0)
                FA addbit(.A(a[0]), .B(bf), .Cin(subAdd), .S(y[0]), .Cout(carry[0]));
            else if(i < width-1)
                FA addbit(.A(a[i]), .B(bf), .Cin(carry[i-1]), .S(y[i]), .Cout(carry[i]));
            else
                FA addbit(.A(a[i]), .B(bf), .Cin(carry[i-1]), .S(y[i]), .Cout(overflow));
        end
    endgenerate
endmodule
