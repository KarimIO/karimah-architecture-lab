# Architecture Lab 4 - Data Path
A parameterized data path in verilog.

# Usage
## Windows
Compile the individual *_tb.v in iVerilog. I'll upload a .bat file to do this automatically later.

## Linux / Mac
Use the bash script to test individual testbenches:
```bash
    ./*.sh
```

Use the bash script to build all verilog files:
```bash
    ./buildall.sh
```

To view the waveform, either open the generated output/*.vcd file in GTKWave.

# Requirements
IcarusVerilog, GTKWave

# License
N/A.