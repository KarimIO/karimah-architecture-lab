`timescale 1ns/1ns

`include "../Lab4/adder.v"

module Adder_tb();
    parameter width = 32;

    // Inputs
    reg [width-1:0]a;
    reg [width-1:0]b;
    reg subAdd;

    // Outputs
    wire [width-1:0]y;
    wire overflow;

   adder #(width) UUT(.a(a), .b(b), .subAdd(subAdd), .y(y), .overflow(overflow));

    wire [width-1:0] checker;
    wire error;
    integer i;
    initial begin
        a = 0;
        b = 0;
        subAdd = 0;
        for(i = 0; i < 15; i++) begin
            a = $random;
            b = $random;
            subAdd = $random%2;
            #5;
        end
    end

    assign checker = subAdd?(a-b):(a+b);
    assign error = ~(checker == y);

    initial begin
        $dumpfile("output/adder.vcd");
        $dumpvars;
    end
endmodule