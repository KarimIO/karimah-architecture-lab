`timescale 1ns/1ns

`include "../Lab4/mux2.v"

module mux4(d0, d1, d2, d3, s, y);
    parameter width = 32;
    input [width-1:0]d0, d1, d2, d3;
    input [1:0]s;
    output [width-1:0]y;
    
    wire [width-1:0] o1, o2;

    mux2 #(width) a(d0, d1, s[0], o1);
    mux2 #(width) b(d2, d3, s[0], o2);
    mux2 #(width) c(o1, o2, s[1], y);
endmodule