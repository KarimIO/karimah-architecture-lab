`timescale 1ns/1ns

`include "../Lab4/flopr_param.v"

module flopr_param_tb();
    parameter width = 32;

    // Inputs
    reg clk, rst;
    reg [width-1:0]d;

    // Outputs
    wire [width-1:0]q;

    flopr_param #(width) UUT(.clk(clk), .rst(rst), .d(d), .q(q));

    integer i;
    initial begin
        clk=0;
        rst=1;
        #5;
        rst=0;
        for(i=0; i < 15; i++) begin
            d=$random;
            clk=~clk;
            #5;
        end
    end

    initial begin
        $dumpfile("output/flopr_param.vcd");
        $dumpvars;
    end
endmodule