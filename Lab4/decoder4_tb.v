`timescale 1ns/1ns

`include "../Lab4/decoder4.v"

module decode4_tb();

    // Inputs
    reg [3:0]a;

    // Outputs
    wire [15:0]y;

    decoder4 UUT(.a(a), .y(y));

    integer i;
    initial begin
        a = 0;
        for(i = 0; i < 15; i++) begin
            a = $random;
            #10;
        end
    end

    initial begin
        $dumpfile("output/decoder4.vcd");
        $dumpvars;
    end
endmodule