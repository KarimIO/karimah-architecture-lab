`timescale 1ns/1ns

module rom(adr, dout);
    parameter depth = 128;

    input [31:0] adr;
    output [31:0] dout;
    
    reg [31:0]mem[depth-1:0];

    initial begin
        $readmemh("rom.txt", mem);
    end

    assign dout = mem[adr/4];
endmodule