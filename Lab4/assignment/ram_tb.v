`timescale 1ns/1ns

`include "../Lab4/assignment/ram.v"

module ram_tb();
    parameter depthBits = 32;
    parameter width = 32;

    reg clk, we;
    reg [depthBits-1:0] adr;
    reg [width-1:0] din;
    wire [width-1:0] dout;

    ram UUT(clk, we, adr, din, dout);
    
    integer i;
    initial begin
        clk = 0;
        #5;
        we = 1;
        for(i = 0; i < 4; i=i+1) begin
            clk = ~clk;
            adr = i*4;
            din = $random;
            #5;
            clk = ~clk;
            #5;
        end
        we = 0;
        for(i = 0; i < 4; i=i+1) begin
            clk = ~clk;
            adr = i*4;
            #5;
            clk = ~clk;
            #5;
        end
    end

    initial begin
        $dumpfile("output/ram.vcd");
        $dumpvars;
    end
endmodule