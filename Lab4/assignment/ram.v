`timescale 1ns/1ns

module ram(clk, we, adr, din, dout);
    parameter depth = 128;
    parameter depthBits = 32;
    parameter width = 32;

    input clk, we;
    input [depthBits-1:0] adr;
    input [width-1:0] din;
    output reg [width-1:0] dout;
    
    reg [width-1:0]mem[depth-1:0];

    integer i;
    initial begin
        for(i = 0; i < 32; i = i+1) begin
            mem[i] <= 0;
        end
    end

    always @ (posedge clk) begin
        if (we && adr != 0) begin
            mem[adr] <= din;
            dout <= din;
        end
        else
            dout <= mem[adr];
    end
endmodule