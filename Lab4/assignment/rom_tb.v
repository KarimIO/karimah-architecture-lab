`timescale 1ns/1ns

`include "../Lab4/assignment/rom.v"

module rom_tb();
    parameter depth = 8;
    parameter depthBits = 32;
    parameter width = 32;

    reg clk, rst;
    reg [depthBits-1:0] adr;
    reg [width-1:0] din;
    wire [width-1:0] dout;

    rom UUT(clk, rst, adr, dout);
    
    integer i;
    initial begin
        clk = 0;
        rst = 1;
        #5;
        rst = 0;
        for(i = 0; i < depth; i=i+1) begin
            clk = ~clk;
            adr = i;
            #5;
            clk = ~clk;
            #5;
        end
    end

    initial begin
        $dumpfile("output/rom.vcd");
        $dumpvars;
    end
endmodule