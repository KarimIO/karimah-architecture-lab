`timescale 1ns/1ns

module registerfile32(clk, we, ra1, ra2, wa, wd, rd1, rd2);
    input clk, we;
    input [4:0] ra1, ra2, wa;
    input [31:0] wd;
    output [31:0] rd1, rd2;

    reg [31:0]registers[31:0];

    integer i;
    initial begin
        for (i = 0; i < 32; i=i+1)
            registers[i] = 0;
    end

    always @ (posedge clk) begin
        if (we)
            registers[wa] <= wd;
    end
    
    assign rd1 = registers[ra1];
    assign rd2 = registers[ra2];
endmodule