`timescale 1ns/1ns

`include "../Lab4/registerfile32.v"

module registerfile32_tb();
    // Inputs
    reg clk;
    reg we;
    reg [31:0]ra1, ra2, wa;
    reg [31:0]wd;
    
    // Outputs
    wire [31:0]rd1, rd2;

    registerfile32 UUT(.clk(clk), .we(we), .ra1(ra1), .ra2(ra2), .wa(wa), .wd(wd), .rd1(rd1), .rd2(rd2));

    integer i;
    initial begin

        for (i = 0; i < 6; i=i+1) begin
            clk = 1;
            we = $random%2;
            ra2 = wa;
            wa = $unsigned($random)%32;
            wd = $random;
            ra1 = wa;
            #5;
            clk = 0;
            #5;
        end
    end

    initial begin
        $dumpfile("output/regfile32.vcd");
        $dumpvars;
    end
endmodule