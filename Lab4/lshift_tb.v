`timescale 1ns/1ns

`include "../Lab4/lshift.v"

module lshift_tb();

    // Inputs
    reg [31:0]a;

    // Outputs
    wire [31:0]y;

    lshift UUT(.a(a), .y(y));

    wire [31:0] checker;
    wire error;

    integer i;
    initial begin
        a = 0;
        for(i = 0; i < 15; i++) begin
            a = $random;
            #10;
        end
    end

    assign checker = a * 4;
    assign error = ~(checker == y);

    initial begin
        $dumpfile("output/lshift.vcd");
        $dumpvars;
    end
endmodule