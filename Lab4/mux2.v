`timescale 1ns/1ns

module mux2(d0, d1, s, y);
    parameter width = 32;
    input [width-1:0]d0, d1;
    input s;
    output [width-1:0]y;

    wire iw;
    not iv(iw, s);

    genvar i;
    generate
        for(i = 0; i < width; i=i+1) begin: wires
            wire wp, wn;
            and ap(wp, d0[i], iw);
            and an(wn, d1[i], s);
            or o(y[i], wp, wn);
        end
    endgenerate
endmodule