`timescale 1ns/1ns

`include "../Lab4/signext.v"

module signext_tb();

    // Inputs
    reg [15:0]a;

    // Outputs
    wire [31:0]y;

    signext UUT(.a(a), .y(y));

    wire error;
    integer i;
    initial begin
        a = 0;
        for(i = 0; i < 15; i=i+1) begin
            a = $random;
            #10;
        end
    end

    assign error = ($signed(y)!=$signed(a));

    initial begin
        $dumpfile("output/signext.vcd");
        $dumpvars;
    end
endmodule