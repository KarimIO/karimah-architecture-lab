`timescale 1ns/1ns

module flopr_param(clk, rst, d, q);
    parameter width = 32;
    input clk, rst;
    input [width-1:0] d;
    output reg [width-1:0] q;

    always @ (posedge clk, posedge rst) begin
        if (rst)
            q <= 32'b0;
        else
            q <= d;
    end
endmodule